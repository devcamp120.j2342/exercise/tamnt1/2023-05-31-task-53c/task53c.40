import model.Circle;
import model.Rectangle;

public class App {
    public static void main(String[] args) throws Exception {
        Circle cirle3 = new Circle(3.0);
        System.out.println(cirle3);
        System.out.println(cirle3.getArea() + " and " + cirle3.getPerimeter());
        Rectangle rectangle3 = new Rectangle(2.5, 1.5);
        System.out.println(rectangle3);
        System.out.println(rectangle3.getArea() + " and " + rectangle3.getPerimeter());
    }
}
